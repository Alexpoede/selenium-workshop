package ro.wantsome.exercises.selenium;

import org.apache.commons.lang3.SystemUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static seleniumintro.DriversUtils.setWebDriveProperty;

public class IntroductionExercises {

    private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        setWebDriveProperty("chrome");
        driver = new ChromeDriver();
    }


    /**
     * Start a new Chrome instance. Navigate to http://practica.wantsome.ro/blog/
     * and perform the following operations:
     * - print the page URL
     * - print the page title
     * - assert the fact that the page source contains 'Wantsome Iasi'
     */
    @Test
    public void firstBlogTest() throws InterruptedException {
        String pageUrl = "https://practica.wantsome.ro/blog/";
        driver.get(pageUrl);
        System.out.println(driver.getCurrentUrl());
        System.out.println(driver.getTitle());
        Assert.assertTrue(driver.getPageSource().contains("Wantsome Iasi"));
        Thread.sleep(1000);
    }

    /**
     * Start a new Chrome instance. Navigate to http://practica.wantsome.ro/blog/
     * and perform the following actions:
     * - navigate to contact page and check page title
     * - navigate to login page and check that page source contains 'Remember Me'
     * - navigate to register page and check the current URL
     */
    @Test
    public void blogInterrogationTest() throws InterruptedException {
        String pageUrl = "https://practica.wantsome.ro/blog/";
        String contactPageUrl = pageUrl + "contact/";
        String loginPageUrl = pageUrl + "login/";
        String registerPageUrl = pageUrl + "register/";


        driver.get(pageUrl);
        WebDriver.Navigation navigation = driver.navigate();
        navigation.to(contactPageUrl);
        Thread.sleep(2000);
        navigation.to(loginPageUrl);
        Thread.sleep(2000);
        Assert.assertTrue(driver.getPageSource().contains("Remember Me"));
        navigation.to(registerPageUrl);
        Thread.sleep(2000);
        String currentPageUrl = driver.getCurrentUrl();
        Assert.assertTrue(currentPageUrl.equals(registerPageUrl));

    }

    /**
     * Start a new Chrome instance. Navigate to http://practica.wantsome.ro/blog/
     * and perform the following operations:
     * - close the browser
     * - start again the browser and open the same URL
     * - navigate to Login page and check that you are on Login page (you have to think about a solution for this)
     * - navigate back and check you are again on homepage
     * - navigate to Register page
     * - go back
     * - navigate forward and check you are on Register page
     * - go back
     * - refresh the page and check you are on homepage
     * - close the browser
     */
    @Test
    public void navigationTest() {
        String pageUrl = "https://practica.wantsome.ro/blog/";
        String registerPageUrl = pageUrl + "register/";
        String loginPageUrl = pageUrl + "login/";
        WebDriver.Navigation navigation = driver.navigate();
        driver.get(pageUrl);
        driver.close();
        driver = new ChromeDriver();
        navigation = driver.navigate();
        driver.get(pageUrl);

        navigation.to(loginPageUrl);
        String pageSource = driver.getPageSource();
        Assert.assertTrue(pageSource.contains("Login"));
        Assert.assertTrue(pageSource.contains("Username or Email"));
        Assert.assertTrue(pageSource.contains("Password"));
        Assert.assertTrue(pageSource.contains("Remember Me"));
        Assert.assertTrue(pageSource.contains("Log In"));

        navigation.back();
        Assert.assertTrue(driver.getCurrentUrl().equals(pageUrl));

        navigation.to(registerPageUrl);
        navigation.back();
        navigation.forward();
        Assert.assertTrue(driver.getCurrentUrl().equals(registerPageUrl));

        navigation.back();
        navigation.refresh();
        Assert.assertTrue(driver.getCurrentUrl().equals(pageUrl));

        driver.close();
    }

    @AfterClass
    public static void teardown() {
        try {
            driver.close();
        }
       catch (Exception ex) {
            System.out.println("Driver session already closed.");
       }
    }
}




