package shop.pages;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static shop.utils.DriversUtils.setWebDriveProperty;

public class WhiteLotus {

    protected static WebDriver driver;

    @BeforeClass
    public static void setup() {
        setWebDriveProperty("chrome");
        driver = new ChromeDriver();
    }

    @Test
    public void testWhiteLotus()  {
        driver.navigate().to("http://whitelotus.cristiancotoi.ro/");
        ((JavascriptExecutor) driver).executeScript("window.localStorage.setItem('Test123','stefanescu.alexcatalina@gmail.com')");
        driver.navigate().to("http://whitelotus.cristiancotoi.ro/calendar");
    }

}
