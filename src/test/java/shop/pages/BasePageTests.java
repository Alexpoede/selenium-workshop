package shop.pages;

import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URLClassLoader;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import static shop.utils.DriversUtils.setWebDriveProperty;

public class BasePageTests {
    protected String root = "https://practica.wantsome.ro/shop/";
    protected static WebDriver driver;
    protected WebDriverWait wait;

    private ResourceBundle pageResourceBundle;
    private ResourceBundle headerResourceBundle;

    public BasePageTests() {
        //this.headerResourceBundle = ResourceBundle.getBundle("Header");
    }

    protected BasePageTests(String resourceBundleName) {
        //set resource bundle
        //this.pageResourceBundle = ResourceBundle.getBundle(resourceBundleName);
    }

    protected String getLocalizedString(String resourceName) {
        return pageResourceBundle.getString(resourceName);
    }

    private String getLocalizedString(ResourceBundle bundle, String resourceName) {
        return bundle.getString(resourceName);
    }

    private void checkMenuItemNameAndUrl(List<WebElement> menuElements, int itemIndex, String itemDisplayedLabel, String redirectUrl) {
        WebElement homeMenuItem = menuElements.get(itemIndex).findElement(By.tagName("a"));
        assertEquals(itemDisplayedLabel, homeMenuItem.getText());
        assertEquals(redirectUrl, homeMenuItem.getAttribute("href"));
    }

    @BeforeClass
    public static void setup() {
        setWebDriveProperty("chrome");
        driver = new ChromeDriver();
    }

    @Before
    public void setLocale() {
        Locale.setDefault(new Locale("en", "US"));
        wait = new WebDriverWait(driver, 10);
    }


    @Test
    public void validateHeaderSectionLabels() {
        driver.get(root);
        String siteTitleXPath = "//a[contains(text(),'Wantsome Shop')]";
        String siteDescriptionXPath = "//p[@id='site-description']";
        String myCartXPath = "//div[@class='my-cart']";

        WebElement siteTitle = driver.findElement(By.xpath(siteTitleXPath));
        WebElement siteDescription = driver.findElement(By.xpath(siteDescriptionXPath));
        WebElement myCart = driver.findElement(By.xpath(myCartXPath));
        Assert.assertEquals("Wantsome Shop", siteTitle.getText());
        Assert.assertEquals("Wantsome? Come and get some!", siteDescription.getText());
        Assert.assertEquals("TOTAL", myCart.getText());
    }

    @Test
    public void validateHeaderEmptyCartDisplay() {
        driver.get(root);

        String myCartXPath = "//div[@class='my-cart']";
        String cartProductsXPath = "//div[@class='widget woocommerce widget_shopping_cart']";
        String cartNoProductsXPath = "//div[@class='widget woocommerce widget_shopping_cart']//li[@class='empty']";
        //String viewCartButtonXPath = "//div[@class='widget woocommerce widget_shopping_cart']//a[@class='button wc-forward'][contains(text(),'View cart')]";

        WebElement myCart = driver.findElement(By.xpath(myCartXPath));
        WebElement cartProducts = driver.findElement(By.xpath(cartProductsXPath));
        WebElement cartNoProducts = driver.findElement(By.xpath(cartNoProductsXPath));
        // WebElement viewCartButton = driver.findElement(By.xpath(viewCartButtonXPath));

        Assert.assertFalse(cartProducts.isDisplayed());
        Assert.assertFalse(cartNoProducts.isDisplayed());

        Actions builder = new Actions(driver);
        builder.moveToElement(myCart).perform();

        assertTrue(cartProducts.isDisplayed());
        assertTrue(cartNoProducts.isDisplayed());
        Assert.assertEquals("No products in the cart.", cartNoProducts.getText());
        // Assert.assertTrue(viewCartButton.isDisplayed());
    }


    @Test
    public void validateCartButtonRedirectToCartPage() {
        driver.get(root);

        String cartRedirectButtonXPath = "//a[@class='wcmenucart-contents']";
        String cartPageBreadcrumbXPath = "//div[@id='crumbs']";
        String cartPageBreadcrumbCurrentXPath = "//span[@class='current']";
        WebElement cartRedirectButton = driver.findElement(By.xpath(cartRedirectButtonXPath));

        cartRedirectButton.click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(cartPageBreadcrumbXPath)));

        WebElement cartPageBreadcrumb = driver.findElement(By.xpath(cartPageBreadcrumbXPath));
        WebElement cartPageBreadcrumbCurrent = cartPageBreadcrumb.findElement(By.xpath(cartPageBreadcrumbCurrentXPath));

        Assert.assertEquals("Cart", cartPageBreadcrumbCurrent.getText());
    }

    @Test
    public void validateHeaderEmptyCartTotalAmount() {
        driver.get(root);

        String myCartSelectedItemsCountXPath = "//span[@class='cart-value']";
        String myCartTotalAmountXPath = "//div[@class='cart-total']";

        WebElement myCartSelectedItemsCount = driver.findElement(By.xpath(myCartSelectedItemsCountXPath));
        WebElement myCartTotalAmount = driver.findElement(By.xpath(myCartTotalAmountXPath));

        Assert.assertEquals("0", myCartSelectedItemsCount.getText());
        Assert.assertEquals("0,00 lei", myCartTotalAmount.getText());
    }

    @Test
    public void validateHeaderImage() {
        String headerImageXPath = "//a[@class='single_image_with_link']//img";
        driver.get(root);
        WebElement headerImage = driver.findElement(By.xpath(headerImageXPath));

        Assert.assertEquals("http://www.fiberwoodart.com/wp-content/uploads/2017/04/header-2.jpg", headerImage.getAttribute("src"));
    }

    @Test
    public void selectTest() {
        driver.navigate()
                .to("https://practica.wantsome.ro/shop");

        WebElement sortDropdown = driver.findElement(By.cssSelector("select.orderby"));
        Select sortSelect = new Select(sortDropdown);
        sortSelect.selectByIndex(2);
        assertTrue(driver.getCurrentUrl().contains("orderby=rating"));
    }

    @Test
    public void verifyCartElementButton() throws InterruptedException {
        driver.navigate().to("https://practica.wantsome.ro/shop");
        By specialXpath = getProductCartButton("Men’s watch");
        WebElement addToCartButton = driver.findElement(specialXpath);
        scrollToElement(addToCartButton);
        addToCartButton.click();

        WebElement cartTopElement = driver.findElement(By.xpath("//a[@class='wcmenucart-contents']"));
        scrollToElement(cartTopElement);

        new Actions(driver).moveToElement(driver.findElement(By.xpath("//a[@class='wcmenucart-contents']"))).build().perform();


        String xpathLocatorViewCard = "//div[@class='widget woocommerce widget_shopping_cart']//a[@class='button wc-forward'][contains(text(),'View cart')]";
        WebElement viewCartElement = driver.findElement(By.xpath(xpathLocatorViewCard));
        assertTrue(viewCartElement.isDisplayed());

    }

    public void scrollToElement(WebElement addToCartButton) throws InterruptedException {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", addToCartButton);
        Thread.sleep(500);
    }

    public void scrollX(WebElement addToCartButton) throws InterruptedException {
        ((JavascriptExecutor) driver).executeScript("scroll(scrollX, 0);");
        Thread.sleep(500);
    }

    public By getProductCartButton(String product) {
        return By.xpath("//a[contains(text(),'" + product + "')]/parent::*/parent::*/div/a");
    }

    @Test
    public void cookiesTest() {
        driver.navigate().to("https://practica.wantsome.ro/shop");
        Set<Cookie> someCookies = driver.manage().getCookies();
        driver.manage().addCookie(new Cookie("Wantsome cookies?", "Yes, please!"));
        assertEquals(2, driver.manage().getCookies().size());
        driver.manage().deleteAllCookies();
    }

    @Test
    public void testDropDown() {
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        //driver.manage().timeouts().pageLoadTimeout(4, TimeUnit.SECONDS);
        // driver.manage().timeouts().setScriptTimeout(4, TimeUnit.SECONDS);
        driver.navigate().to("https://singdeutsch.com/");
        By titleXpath = By.xpath("//h1[@class='site-title h1']");
        wait.until(ExpectedConditions.elementToBeClickable(titleXpath));
        assertEquals("Sing Deutsch".toUpperCase(), driver.findElement(titleXpath).getText());

    }

    @Test
    public void testSlowLoadingElement() {
        driver.navigate().to("https://singdeutsch.com/");
        By titleXpath = By.xpath("//h1[@class='site-title h1']");
        WebElement titleElement = wait.until(ExpectedConditions.elementToBeClickable(titleXpath));
        assertEquals("Sing Deutsch".toUpperCase(), titleElement.getText());

    }


    @Test
    public void validateMenuElements() {
        driver.get(root);
        String blogPageUrl = root + "?page_id=529";
        String cartPageUrl = root + "?page_id=9";
        String checkoutPageUrl = root + "?page_id=10";
        String connectPageUrl = root + "?page_id=560";

        List<WebElement> siteNavigationMenuElements = driver.findElements(By.cssSelector("#site-navigation .menu-item"));

        assertEquals(5, siteNavigationMenuElements.size());
        checkMenuItemNameAndUrl(siteNavigationMenuElements, 0, "HOME", root);

        checkMenuItemNameAndUrl(siteNavigationMenuElements, 1, "BLOG", blogPageUrl);

        checkMenuItemNameAndUrl(siteNavigationMenuElements, 2, "CART", cartPageUrl);

        checkMenuItemNameAndUrl(siteNavigationMenuElements, 3, "CHECKOUT", checkoutPageUrl);

        checkMenuItemNameAndUrl(siteNavigationMenuElements, 4, "CONNECT", connectPageUrl);
    }

    @Test
    public void categoryMenuElements() {
        driver.get(root);
        String categoryMenuXPath = "//div[@class='category-menu']";
        String categoryNavigationSelector = "#category-navigation";
        String categoryMenuItemsSelector = "#category-menu .menu-item";
        String menCollectionUrl = root + "?product_cat=men-collection";
        String womenCollectionUrl = root + "?product_cat=women-collection";

        WebElement categoryMenu = driver.findElement(By.xpath(categoryMenuXPath));

        assertNotNull(categoryMenu);
        WebElement categoryNavigation = categoryMenu.findElement(By.cssSelector(categoryNavigationSelector));

        assertNotNull(categoryNavigation);
        assertFalse(categoryNavigation.isDisplayed());
        categoryMenu.click();

        assertTrue(categoryNavigation.isDisplayed());


        List<WebElement> categoryMenuItems = categoryNavigation.findElements(By.cssSelector(categoryMenuItemsSelector));
        wait.until(ExpectedConditions.visibilityOfAllElements(categoryMenuItems));
        assertEquals(2, categoryMenuItems.size());
        checkMenuItemNameAndUrl(categoryMenuItems, 0, "Men Collection", menCollectionUrl);
        checkMenuItemNameAndUrl(categoryMenuItems, 1, "Women Collection", womenCollectionUrl);
    }


    @Test
    public void categoryMenuMenItem() {
        driver.get(root);
        String categoryMenuXPath = "//div[@class='category-menu']";
        String categoryNavigationSelector = "#category-navigation";
        String categoryMenuItemsSelector = "#category-menu .menu-item";

        WebElement categoryMenu = driver.findElement(By.xpath(categoryMenuXPath));

        assertNotNull(categoryMenu);
        WebElement categoryNavigation = categoryMenu.findElement(By.cssSelector(categoryNavigationSelector));

        assertNotNull(categoryNavigation);
        assertFalse(categoryNavigation.isDisplayed());
        categoryMenu.click();

        assertTrue(categoryNavigation.isDisplayed());

        List<WebElement> categoryMenuItems = categoryNavigation.findElements(By.cssSelector(categoryMenuItemsSelector));
        wait.until(ExpectedConditions.visibilityOfAllElements(categoryMenuItems));
        assertEquals(2, categoryMenuItems.size());
        categoryMenuItems.get(0).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("entry-title")));
        WebElement menPageEntryTitle = driver.findElement(By.className("entry-title"));


        Assert.assertEquals("Men Collection", menPageEntryTitle.getText());
    }
    @Test
    public void categoryMenuWomenItem() {
        driver.get(root);
        String categoryMenuXPath = "//div[@class='category-menu']";
        String categoryNavigationSelector = "#category-navigation";
        String categoryMenuItemsSelector = "#category-menu .menu-item";

        WebElement categoryMenu = driver.findElement(By.xpath(categoryMenuXPath));

        assertNotNull(categoryMenu);
        WebElement categoryNavigation = categoryMenu.findElement(By.cssSelector(categoryNavigationSelector));

        assertNotNull(categoryNavigation);
        assertFalse(categoryNavigation.isDisplayed());
        categoryMenu.click();

        assertTrue(categoryNavigation.isDisplayed());

        List<WebElement> categoryMenuItems = categoryNavigation.findElements(By.cssSelector(categoryMenuItemsSelector));
        wait.until(ExpectedConditions.visibilityOfAllElements(categoryMenuItems));
        assertEquals(2, categoryMenuItems.size());
        categoryMenuItems.get(1).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("entry-title")));
        WebElement womenPageEntryTitle = driver.findElement(By.className("entry-title"));

        Assert.assertEquals("Women Collection", womenPageEntryTitle.getText());
    }
    @Test
    public void validateUserButtonRedirectToMyAccountPage() {
        driver.get(root);

        String accountButtonXPath = "//div[@class='user-wrapper search-user-block']";
        String accountPageBreadcrumbXPath = "//div[@id='crumbs']";
        String accountPageBreadcrumbCurrentXPath = "//span[@class='current']";
        WebElement accountRedirectButton = driver.findElement(By.xpath(accountButtonXPath));

        accountRedirectButton.click();

        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(accountPageBreadcrumbXPath)));

        WebElement accountPageBreadcrumb = driver.findElement(By.xpath(accountPageBreadcrumbXPath));
        WebElement accountPageBreadcrumbCurrent = accountPageBreadcrumb.findElement(By.xpath(accountPageBreadcrumbCurrentXPath));

        Assert.assertEquals("My account", accountPageBreadcrumbCurrent.getText());
    }

    @AfterClass
    public static void teardown() {
        driver.close();
    }
}
