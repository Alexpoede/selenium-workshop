package locators;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static seleniumintro.DriversUtils.setWebDriveProperty;


public class LocatorsTests {

    private static WebDriver driver;

    public void firstBlogTest() throws InterruptedException {
        setWebDriveProperty("chrome");
        driver.get("https://practica.wantsome.ro/blog/");


    }


    @BeforeClass
    public static void setup() {
        setWebDriveProperty("chrome");
        driver = new ChromeDriver();
    }

    @Test

    public void findByCss() throws InterruptedException {
        WebElement searchBox = driver.findElement(By.cssSelector(".search-field"));
        assertEquals(searchBox.getAttribute("title"), "Search for:");
        assertEquals("", searchBox.getText());
        searchBox.sendKeys("Alphabet");
        assertEquals("", searchBox.getText());
    }


    @Test
    public void findByTopTitle(){
        WebElement mainTitle = driver.findElement(By.xpath("//a[@rel='home']"));
        assertEquals("Wantsome Iasi", mainTitle.getText());
        assertTrue(mainTitle.getAttribute("href").contains("wantsome.ro"));
        //assertTrue(mainTitle.getTagName())
    }

    @AfterClass
    public static void teardown() {
        try {
            driver.close();
        }
        catch (Exception ex) {
            System.out.println("Driver session already closed.");
        }
    }
}

