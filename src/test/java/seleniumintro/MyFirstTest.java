package seleniumintro;

import org.apache.commons.lang3.SystemUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;
import static seleniumintro.DriversUtils.setWebDriveProperty;


public class MyFirstTest {
    @Test
    public void openBrowser() throws InterruptedException {
        setWebDriveProperty("chrome");
        WebDriver driver = new ChromeDriver();

        driver.get("https://practica.wantsome.ro/blog/");
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        Thread.sleep(5000);
        //tear down method
        driver.close();
    }

    @Test
    public void openFirefox() throws InterruptedException {
        setWebDriveProperty("firefox");
        WebDriver driver = new FirefoxDriver();

        driver.get("https://practica.wantsome.ro/blog/");
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        Thread.sleep(5000);
        //tear down method
        driver.close();
    }

    @Test
    public void openHtmlUnitDriver() throws InterruptedException {
        //System.setProperty("webdriver.html.driver");
        WebDriver driver = new HtmlUnitDriver();

        driver.get("https://practica.wantsome.ro/blog/");
        String pageTitle = driver.getTitle();
        System.out.println(pageTitle);
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        Thread.sleep(5000);
        //tear down method
        driver.close();
    }

    private static WebDriver driver;

    @BeforeClass
    public static void setup() {
        setWebDriveProperty("chrome");
        driver = new ChromeDriver();
    }

    @Test
    public void openChromeBrowser() throws InterruptedException {
        //System.setProperty("webdriver.html.driver")

        driver.get("https://practica.wantsome.ro/blog/");
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());

        driver.navigate().to("https://www.emag.ro/");
        assertNotEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());
        assertEquals("eMAG.ro - Libertate în fiecare zi", driver.getTitle());

        driver.navigate().back();
        assertEquals("Wantsome Iasi – Practice website for testing sessions", driver.getTitle());

        driver.navigate().forward();
        assertEquals("eMAG.ro - Libertate în fiecare zi", driver.getTitle());
    }


    @Test

    public void findByCss() throws InterruptedException {
        setWebDriveProperty("chrome");
        driver.get("https://practica.wantsome.ro/blog/");

        WebElement searchBox = driver.findElement(By.cssSelector(".search-field"));
        Assert.assertEquals(searchBox.getAttribute("title"), "Search for:");
        Assert.assertEquals("", searchBox.getText());
        searchBox.sendKeys("Alphabet");
        Assert.assertEquals("", searchBox.getText());
    }

    /*public void findTitleOfPost() {
        List<WebElement> postTitle = driver.findElements(By.cssSelector("header>h2.entry-title"));
        for(WebElement element:postTitle)
        assertThat(element.getText(), contains("Test"));*/

    /*@Test
    public void findXPath() {
        WebElement searchBox = driver.findElements(By.xpath("//div[@id='logo']/h1/a[text()='Wantsome Iasi']"));


    }*/

    @Test
    public void findXPathLogo() {
        WebElement searchBox = driver.findElement(By.cssSelector(".search-field"));
        WebElement searchButton = driver.findElement(By.className("search-submit"));

        searchBox.sendKeys("lorem");
        //searchBox.sendKeys(Keys.ENTER);
        searchButton.click();

        //The search page will load
        WebElement searchResultsLabel = driver.findElement(By.xpath("//h1[@class='archive-title']"));
        assertTrue(searchResultsLabel.getText().contains("Search Results"));

    }

    /*@AfterClass
    public static void teardown() {
        driver.close();
    }*/
}


