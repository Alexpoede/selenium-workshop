package seleniumintro;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static seleniumintro.DriversUtils.setWebDriveProperty;

    public class TestClass {

        private static WebDriver driver;

        @BeforeClass
        public static void setup() {
            setWebDriveProperty("chrome");
            driver = new ChromeDriver();
        }

        @Test
        public void pageSourceTest() throws InterruptedException {
            driver.get("https://practica.wantsome.ro/blog/");
            System.out.println(driver.getPageSource());

            System.out.println("Currebt url is: " + driver.getCurrentUrl());
            System.out.println("Currebt title is: " + driver.getTitle());
        }

        @Test
        public void testTitle() {
            driver.get("https://practica.wantsome.ro/blog/");

            String title = driver.getTitle();
            System.out.println("Now title is" + title);
            String[] words = title.split(" ");
            System.out.println("After split array is: " + Arrays.toString(words));
            int numberOfWordsWithMoreThan5Letters = 0;
            for (String word : words) {
                if (word.length() > 5) {
                    numberOfWordsWithMoreThan5Letters++;
                }
            }

            assertEquals(5, numberOfWordsWithMoreThan5Letters);
        }

        @Test
        public void testTextPressentInSourcePage() {
            driver.get("https://practica.wantsome.ro/blog/");

            assertTrue(driver.getPageSource().contains("The roof is on fireeee"));



        }

        @AfterClass
        public static void teardown() {
            driver.close();
        }


    }

